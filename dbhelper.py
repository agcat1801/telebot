import sqlite3


class DBHelper:
    def __init__(self, dbname="todo.sqlite"):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)

    def setup(self):
        stmt = "CREATE TABLE IF NOT EXISTS data_item (id_tele int, name text, cell_row int, is_img int, type_chat text)"
        self.conn.execute(stmt)
        self.conn.commit()

    def setup_shipper(self):
        stmt = "CREATE TABLE IF NOT EXISTS shipper (name text, shipper_type text)"
        self.conn.execute(stmt)
        self.conn.commit()

    # shipper_type noty : not yet
    # shipper_type wait : avaible
    # shipper_type done : finish

    def update_shipper_type(self, name, shipper_type):
        stmt = "UPDATE shipper SET shipper_type = ? WHERE name = ?"
        args = (shipper_type, name)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def add_shipper(self, name, type_chat):
        stmt = "INSERT INTO shipper (name, shipper_type) VALUES (?, ?)"
        args = (name, type_chat)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def get_shipper_all_list(self):
        stmt = "SELECT * FROM shipper"
        return self.conn.execute(stmt)


    def get_all_data_user(self):
        stmt = "SELECT * FROM data_item"
        return self.conn.execute(stmt)

    def get_shipper_wait_list(self):
        stmt = "SELECT * FROM shipper WHERE shipper_type = 'wait'"
        return self.conn.execute(stmt)

    def get_shipper_done_list(self):
        stmt = "SELECT * FROM shipper WHERE shipper_type = 'done'"
        return self.conn.execute(stmt)

    def recyle_shipper_done_list(self):
        stmt = "UPDATE shipper SET shipper_type = ? WHERE shipper_type = ?"
        args = ("wait", "done")
        self.conn.execute(stmt, args)
        self.conn.commit()

    def recyle_all_shipper_list(self):
        stmt = "UPDATE shipper SET shipper_type = ?"
        args = ("noty")
        self.conn.execute(stmt, args)
        self.conn.commit()

    def delete_all_shipper(self):
        stmt = "DELETE FROM shipper"
        self.conn.execute(stmt)
        self.conn.commit()

    def add_item(self, id_tele, name, cell_row, type_chat):
        stmt = "INSERT INTO data_item (id_tele , name, cell_row, type_chat) VALUES (?, ?, ?, ?)"
        args = (id_tele, name, cell_row, type_chat)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def update_item(self, id_tele, name, cell_row):
    	stmt = "UPDATE data_item SET name = ?, cell_row = ? WHERE id_tele = ?"
    	args = (name, cell_row, id_tele)
    	self.conn.execute(stmt, args)
        self.conn.commit()

    def update_item_img(self, id_tele, is_img):
        stmt = "UPDATE data_item SET is_img = ? WHERE id_tele = ?"
        args = (is_img, id_tele)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def delete_item(self, id_tele):
        stmt = "DELETE FROM data_item WHERE id_tele = (?)"
        args = (id_tele, )
        self.conn.execute(stmt, args)
        self.conn.commit()

    def delete_all_item(self):
        stmt = "DELETE FROM data_item"
        self.conn.execute(stmt)
        self.conn.commit()

    def drop_table(self):
        stmt = "DROP TABLE IF EXISTS data_item"
        self.conn.execute(stmt)
        self.conn.commit()

    def get_items(self):
        stmt = "SELECT * FROM data_item"
        return self.conn.execute(stmt)
