import json 
import simplejson
import requests
import time
import urllib
import gspread
import datetime
from bs4 import BeautifulSoup
import re
import urllib2
import os
from oauth2client.service_account import ServiceAccountCredentials
from dbhelper import DBHelper
import random
import os

# one current task

# Given I am a user of cookbot 
# When I am ordering for a day, if I already have a selection for that day,
# Then cookbot should show me my current selection before it shows me the list of options



SCOPES = ["https://spreadsheets.google.com/feeds"]
TOKEN = "338944393:AAGVjNoooPvK7F5A5sAzt6MNXfxcxfbprAw"
URL = "https://api.telegram.org/bot{}/".format(TOKEN)
FILE_NAME = "CookEpos-9f3a20647631.json"
CANCEL_ORDER = "Remove Current Order"


class Data(object):
    def __init__(self, id_tele, name=None, cell_row=None, is_img=0, type_chat=None):
        self.id_tele = id_tele
        self.type_chat = type_chat
        self.name = name
        self.cell_row = cell_row
        self.is_img = is_img

class Shipper(object):
    def __init__(self, name=None, shipper_type=None):
        if name == None:
            self.name = None
        self.name = name.encode('utf-8')
        self.shipper_type = shipper_type

def get_soup(url,header):
    return BeautifulSoup(urllib2.urlopen(urllib2.Request(url,headers=header)))

def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content


def get_json_from_url(url):
    content = get_url(url)
    print("content")
    print(content)
    try:
        js = json.loads(content)
        return js
    except Exception as e:
        print(e)
        return None

def get_updates(offset=None):
    url = URL + "getUpdates?timeout=50"
    if offset:
        url += "&offset={}".format(offset)
    js = get_json_from_url(url)
    return js

def get_last_update_id(updates):
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)


def add_data(chat_id, type_chat):
    global data_all
    found = False
    for item in data_all:
        if item.id_tele == chat_id:
            found = True
    if found == False:
        data = Data(chat_id, None, None, 0, type_chat)
        data_all.append(data)
        db.add_item(chat_id, None, None, type_chat)

def find_data_by_tele_id(chat_id):
    for item in data_all:
        if item.id_tele == chat_id:
            return item
    return None

def find_cell_by_name(name):
    for item in people_cell_list:
        if item.value.encode('utf-8') == name:
            return item


def api_handler(updates, worksheet):
    for update in updates["result"]:
        try:
            if update.get("callback_query") != None:
                callback_id = update["callback_query"]["id"]
                chat_id = update["callback_query"]["message"]["chat"]["id"]
                data_type = update["callback_query"]["data"][:4].encode('utf-8')

                if data_type == "ship":
                    data_type_shipper = update["callback_query"]["data"][4:8].encode('utf-8')
                    print "data_type_shipper", data_type_shipper
                    data_value_shipper = update["callback_query"]["data"][8:].encode('utf-8')
                    print "data_value_shipper", data_value_shipper

                    db.update_shipper_type(data_value_shipper.decode('utf-8'), data_type_shipper)
                    send_message("Update shipper " + data_value_shipper + " role = " + data_type_shipper, chat_id)
                    return

                if data_type == "user":
                    command = update["callback_query"]["data"][4:7].encode('utf-8')
                    print command
                    if command == "del":
                        id_deleted = update["callback_query"]["data"][7:].encode('utf-8')
                        print id_deleted
                        db.delete_item(id_deleted)
                        send_message("Deleted user with id = " + id_deleted, chat_id)
                        get_all_data_from_database()

                data_day = update["callback_query"]["data"][:3].encode('utf-8')
                data_value = update["callback_query"]["data"][3:].encode('utf-8')

                value_item_list = []
                if data_day == "mon":
                    value_item_list = values_list_mon
                elif data_day == "tue":
                    value_item_list = values_list_tue
                elif data_day == "wed":
                    value_item_list = values_list_wed
                elif data_day == "thu":
                    value_item_list = values_list_thu
                elif data_day == "fri":
                    value_item_list = values_list_fri

                if data_value in value_item_list:
                    data = find_data_by_tele_id(chat_id)
                    if data_value == CANCEL_ORDER:
                        if update_google_sheet("", worksheet, data, data_day):
                            send_message("U cancel order for lunch of " + data_day.upper(), chat_id)
                            send_message_callback(callback_id, "")
                        else:
                            send_message("I dont know who u are. Please type /who to authenticate", chat_id)
                        return

                    if update_google_sheet(data_value, worksheet, data, data_day):
                        send_message("U order "+ data_value + " for lunch of " + data_day.upper(), chat_id)
                        send_message_callback(callback_id, "")
                    else:
                        send_message("I dont know who u are. Please type /who to authenticate", chat_id)
                return

            text = update["message"]["text"].encode('utf-8')
            chat = update["message"]["chat"]["id"]
            type_chat = update['message']['chat']['type']
            print("type_chat")
            print(type_chat)
            add_data(chat, type_chat)
            print("update")
            print(update)
            if type_chat == "group" or type_chat == "supergroup":
                return

            if text == "/mon":
                send_order_by_date(chat, "mon")
            elif text == "/tue":
                send_order_by_date(chat, "tue")
            elif text == "/wed":
                send_order_by_date(chat, "wed")
            elif text == "/thu":
                send_order_by_date(chat, "thu")
            elif text == "/fri":
                send_order_by_date(chat, "fri")
            elif text == "/who":
                keyboard = build_keyboard(get_name_list(), "")
                send_message("Who are you?", chat, keyboard)
            elif text == "/help":
                send_message("/who : to authenticate\n /mon \n /tue \n /wed \n /thu \n /fri \n /all : to check the order list of this week \n /imgon : turn on img when search order \n /imgoff : turn off image \n /changelog : show changelog \n /shipperlist : show and edit shipper status", chat)
            elif text in get_name_list():
                # update data
                data = find_data_by_tele_id(chat)
                cell = find_cell_by_name(text)
                data.name = cell.value
                data.cell_row = cell.row
                db.update_item(chat, data.name, data.cell_row)
                send_message("Welcome " + text + " to CookBot 0.151LS version", chat)
            elif text == "/all":
                data = find_data_by_tele_id(chat)
                if data == None or data.cell_row == None or data.name == None:
                    send_message("I dont know who u are. Please type /who to authenticate", chat)
                else:
                    send_message(data.name.encode('utf-8') + "'s Order For This week:", chat)
                    display_all_order(chat, worksheet, data)
            elif text == "/delete_all":
                db.delete_all_item()
            elif text == "/drop_all":
                db.drop_table()
            elif text == "/update":
                get_all_data_from_gg(worksheet)
            elif text == "/img":
                send_photo_by_search_name("com tam suon", chat)
            elif text == "/imgon":
                data = find_data_by_tele_id(chat)
                if data == None or data.cell_row == None or data.name == None:
                    send_message("I dont know who u are. Please type /who to authenticate", chat)
                else:
                    db.update_item_img(chat, 1)
                    data.is_img = 1
                    send_message("Turn On Img Function", chat)
            elif text == "/imgoff":
                data = find_data_by_tele_id(chat)
                if data == None or data.cell_row == None or data.name == None:
                    send_message("I dont know who u are. Please type /who to authenticate", chat)
                else:
                    db.update_item_img(chat, 0)
                    data.is_img = 0
                    send_message("Turn Off Img Function", chat)
            elif text == "/start":
                send_message("Please type /help for other options \n Changelog 0.13AF: \n * Bot was born \n\n Changelog 0.14FF: \n * Add function to remove order \n\n Changelog 0.151LS: \n * Add function to show image \n * improve server stability", chat)
            elif text == "/changelog":
                # display_all_today_order(chat, worksheet, 2)
                send_message("Please type /help for other options \n Changelog 0.13AF: \n * Bot was born \n\n Changelog 0.14FF: \n * Add function to remove order \n\n Changelog 0.151LS: \n * Add function to show image \n * improve server stability", chat)
            elif text == "/shipperlist":
                send_admin_setting_shipper(chat)
            elif text == "/shipper_recyle":
                db.recyle_all_shipper_list()
            elif text == "/display_all_order_to_group":
                display_all_today_order(chat, worksheet, 2)
            elif text == "/display_all_users":
                display_all_user(chat)
            elif text == "/time":
                now = datetime.datetime.now() + datetime.timedelta(hours=7)
                current_time = now.strftime("%H:%M")
                send_message("Server time : " + current_time, chat)
            else:
                send_message(text + " ? ahihi :'D", chat)
        except Exception as e:
            print(e)



def send_order_by_date(chat, date):
    value_item_list = []
    if date == "mon":
        value_item_list = values_list_mon
    elif date == "tue":
        value_item_list = values_list_tue
    elif date == "wed":
        value_item_list = values_list_wed
    elif date == "thu":
        value_item_list = values_list_thu
    elif date == "fri":
        value_item_list = values_list_fri

    data = find_data_by_tele_id(chat)
    if data != None and data.is_img == 1:
        send_message("Select item u want to order for " + date, chat)
        for item in value_item_list:
            keyboard = build_keyboard_with_img(item, date)
            send_message(item, chat, keyboard)
            if item != CANCEL_ORDER:
                send_photo_by_search_name(item, chat)
    else:
        keyboard = build_keyboard(value_item_list, date, "inline")
        send_message("Select item u want to order for " + date, chat, keyboard)

def send_admin_setting_shipper(chat):
    data_shipper = get_all_data_shipper()
    if len(data_shipper) == 0:
        send_message("Number of shipper = 0 ! Can not do anything", chat)
        return
    keyboard = build_keyboard(data_shipper, None, "admin")
    send_message("Change status for shipper this week :\n noty : not yet \n wait : available \n done : finish shipper job atleast once", chat, keyboard)
    
def get_worksheet():
    credentials = ServiceAccountCredentials.from_json_keyfile_name(FILE_NAME, SCOPES)
    connection = gspread.authorize(credentials)
    worksheet = connection.open("Com Deli").sheet1
    return worksheet

def display_all_user(chat):
    data_users = data_all
    print "display_all_user"
    if len(data_users) == 0:
        send_message("Number of user = 0 ! Can not do anything", chat)
        return
    keyboard = build_keyboard(data_users, None, "admin_data")
    send_message("admin setting", chat, keyboard)
    

def display_all_order(chat, worksheet, data):
    if worksheet == None:
        worksheet = get_worksheet()
    for item in worksheet.range("B" + str(data.cell_row) + ":N" + str(data.cell_row)):
        if item.col in [2, 5, 8, 11, 14]:
            date = ""
            if item.col == 2:
                date = "/mon : "
            elif item.col == 5:
                date = "/tue : "
            elif item.col == 8:
                date = "/wed : "
            elif item.col == 11:
                date = "/thu : "
            elif item.col == 14:
                date = "/fri : "
            send_message(date + item.value.encode('utf-8'), chat)

def display_all_today_order(chat, worksheet, type): ## type = 1: normal, type = 2 shipper
    if worksheet == None:
        worksheet = get_worksheet()
    now = datetime.datetime.now() + datetime.timedelta(hours=7)
    day_of_week = int(now.strftime("%w"))    # 1 2 3 4 5

    array_all = []
    for item in worksheet.range("A22:N39"):
        if item.col == 1 or item.col == (3 * day_of_week - 1):  # 2, 5, 8, 11, 14
            if len(array_all) == 0:
                array  = []
                array.append(item)
                array_all.append(array)
            else:
                found = False
                for array_item in array_all:
                    if len(array_item) > 0:
                        if item.row == array_item[0].row:
                            array_item.append(item)
                            found = True
                if found == False:
                    array  = []
                    array.append(item)
                    array_all.append(array)

    data_shipper = get_all_data_shipper()
    print "len(data_shipper)", len(data_shipper)
    for array_item in array_all:
        string = ""
        empty_value = False
        for item in array_item:
            string = string + " : " +  item.value.encode('utf-8').ljust(8)
            if item.value == "":
                empty_value = True
        if empty_value == False:
            # add ppl to the list
            name_item = array_item[0].value.encode('utf-8')
            found_shipper = False
            if len(data_shipper) > 0:
                for shipper_item in data_shipper:
                    if shipper_item.name == name_item:
                        found_shipper = True
                        if shipper_item.shipper_type == None or shipper_item.shipper_type == "noty":
                            db.update_shipper_type(shipper_item.name.decode('utf-8'), "wait")
            if found_shipper == False:
                db.add_shipper(array_item[0].value, "wait")
            send_message(string, chat)

    # shipper_type noty : not yet
    # shipper_type wait : avaible
    # shipper_type done : finish

    val_total_orders = worksheet.cell( 40, (3 * day_of_week - 1)).value.encode('utf-8')
    val_total_money = worksheet.cell( 40, (3 * day_of_week)).value.encode('utf-8')
    string_total = "Total orders = " + val_total_orders + " | Total Cost Price = " + val_total_money
    send_message(string_total, chat)

    if type == 2:
        choose_time = 0
        while choose_time < 2:
            shipper_list = get_all_data_shipper_wait()
            shipper_list_done = get_all_data_shipper_done()
            print "len(shipper_list)", len(shipper_list)
            print "len(shipper_list_done)", len(shipper_list_done)
            if len(shipper_list) > 0 or len(shipper_list_done) > 0:
                if len(shipper_list) > 0:
                    today_shipper = random.choice(shipper_list)
                    db.update_shipper_type(today_shipper.name.decode('utf-8'), "done")
                    string_shipper = "Today shipper " + str(choose_time + 1) + " is " + today_shipper.name
                    choose_time = choose_time + 1
                    send_message(string_shipper, chat)
                else:
                    send_message("There is no more shipper available, Recycle the list !!!", chat)
                    db.recyle_shipper_done_list()
            else:
                choose_time = 10
                send_message("There is not enough shipper, We gg !!!", chat)


def update_google_sheet(text, worksheet, data, data_day):
    if data != None:
        if data.cell_row != None:
            text = unicode(text, 'utf-8')
            day_string = ""
            if data_day == "mon":
                day_string = "B"
            elif data_day == "tue":
                day_string = "E"
            elif data_day == "wed":
                day_string = "H"
            elif data_day == "thu":
                day_string = "K"
            elif data_day == "fri":
                day_string = "N"

            worksheet.update_acell(day_string + str(data.cell_row), text)
            return True
    return False


def send_message(text, chat_id, reply_markup=None):
    url = URL + "sendMessage?text={}&chat_id={}&parse_mode=Markdown".format(text, chat_id)
    if reply_markup:
        url += "&reply_markup={}".format(reply_markup)
    get_url(url)


def send_message_callback(callback_query_id, text):
    url = URL + "answerCallbackQuery?callback_query_id={}&text={}".format(callback_query_id, text)
    get_url(url)


def send_photo(url, chat_id):
    url = URL + "sendPhoto?photo={}&chat_id={}&parse_mode=Markdown".format(url, chat_id)
    get_url(url)


def send_photo_by_search_name(name, chat_id):
    query = name[2:] + " site:static.wixstatic.com/media"
    if len(name) >= 4:
        if name[-1:] == "k" or name[-1:] == "K":
            query = name[2:-4] + " site:static.wixstatic.com/media"
    query= query.split()
    query='+'.join(query)
    url="https://www.google.com/search?hl=en&site=imghp&tbm=isch&source=hp&biw=1704&bih=883&q="+query
    header = {'User-Agent': 'Mozilla/5.0'} 
    soup = get_soup(url,header)
    images = [a['src'] for a in soup.find_all("img", {"src": re.compile("gstatic.com")})]
    i = 0
    for img in images:
        send_photo(img, chat_id)
        if i == 1:
            break
        i = i + 1


# type = normal, inline
def build_keyboard(items, day, type=None):
    keyboard = []
    reply_markup = {}
    if type == "inline":
        keyboard = [[{"text":item , "callback_data":day + item}] for item in items]
        reply_markup = {"inline_keyboard":keyboard, "remove_keyboard": True}
    elif type == "admin":
        print "admin setting"
        print len(items)
        keyboard = []
        for item in items:
            name = item.name.decode('utf-8')
            string_noty = ""
            string_wait = ""
            string_done = ""
            if item.shipper_type == "noty":
                string_noty = name + " : "
            elif item.shipper_type == "wait":
                string_wait = name + " : "
            else:
                string_done = name + " : "

            keyboard.append([{"text":string_noty + "noty", "callback_data":"shipnoty" + name},
                {"text":string_wait + "wait", "callback_data":"shipwait" + name},
                {"text":string_done + "done", "callback_data":"shipdone" + name}])

        reply_markup = {"inline_keyboard":keyboard, "remove_keyboard": True}

    elif type == "admin_data":
        print "admin data setting"
        print len(items)
        keyboard = []
        for item in items:
            name = "No name"
            if item.name != None:
                name = item.name

            keyboard.append([{"text":name , "callback_data":"user"},
                {"text":item.type_chat, "callback_data":"user" },
                {"text":"delete", "callback_data":"userdel" + str(item.id_tele)}])

        reply_markup = {"inline_keyboard":keyboard, "remove_keyboard": True}

    else:
        keyboard = [[item] for item in items]
        reply_markup = {"keyboard":keyboard, "one_time_keyboard": True}
    return json.dumps(reply_markup)


def build_keyboard_with_img(item, day):
    keyboard = []
    keyboard = [[{"text":item , "callback_data":day + item}],]
    reply_markup = {"inline_keyboard":keyboard, "remove_keyboard": True}
    return json.dumps(reply_markup)


def get_name_list():
    strings = []
    for item in people_cell_list:
        strings.append(item.value.encode('utf-8'))
    return strings


def get_all_data_from_gg(worksheet):
    global values_list_mon
    global values_list_tue
    global values_list_wed
    global values_list_thu
    global values_list_fri
    global people_cell_list

    values_list_mon = []
    values_list_tue = []
    values_list_wed = []
    values_list_thu = []
    values_list_fri = []
    people_cell_list = []

    for x in range(0, 5):
        i = 0
        for item in worksheet.col_values((x * 3) + 2):
            if i <= 12 and i > 0:
                if x == 0:
                    values_list_mon.append(item.encode('utf-8'))
                elif x == 1:
                    values_list_tue.append(item.encode('utf-8'))
                elif x == 2:
                    values_list_wed.append(item.encode('utf-8'))
                elif x == 3:
                    values_list_thu.append(item.encode('utf-8'))
                elif x == 4:
                    values_list_fri.append(item.encode('utf-8'))
            i = i + 1

    values_list_mon.append(CANCEL_ORDER)
    values_list_tue.append(CANCEL_ORDER)
    values_list_wed.append(CANCEL_ORDER)
    values_list_thu.append(CANCEL_ORDER)
    values_list_fri.append(CANCEL_ORDER)
    for item in worksheet.range('A22:A39'):
        people_cell_list.append(item)
    return worksheet


def get_all_data_from_database():
    global data_all
    data_all = []
    items = db.get_items()
    for item in items:
        data = Data(item[0], item[1], item[2], item[3], item[4])
        data_all.append(data)

def get_all_data_shipper_wait():
    data_all_shipper = []
    items = db.get_shipper_wait_list()
    for item in items:
        data = Shipper(item[0], item[1])
        data_all_shipper.append(data)
    return data_all_shipper

def get_all_data_shipper_done():
    data_all_shipper = []
    items = db.get_shipper_done_list()
    for item in items:
        data = Shipper(item[0], item[1])
        data_all_shipper.append(data)
    return data_all_shipper

def get_all_data_shipper():
    data_all_shipper = []
    items = db.get_shipper_all_list()
    for item in items:
        data = Shipper(item[0], item[1])
        data_all_shipper.append(data)
    return data_all_shipper

values_list = []
values_list_mon = []
values_list_tue = []
values_list_wed = []
values_list_thu = []
values_list_fri = []
people_cell_list = []
data_all = []

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

db = DBHelper()


def main():


    db.setup()
    db.setup_shipper()
    # db.delete_all_shipper()
    credentials = ServiceAccountCredentials.from_json_keyfile_name(FILE_NAME, SCOPES)
    connection = gspread.authorize(credentials)
    worksheet = connection.open("Com Deli").sheet1
    worksheet = get_all_data_from_gg(worksheet)
    last_update_id = None
    already_sent = True
    already_sent_money = True
    while True:
        try:
            now = datetime.datetime.now() + datetime.timedelta(hours=7)
            day_of_week = int(now.strftime("%w"))
            current_time = now.strftime("%H:%M")
            # print("----------------")
            # print(len(values_list_mon))
            # print(len(data_all))
            # print(len(people_cell_list))
            # print("---: current time")
            # print(current_time)
            # print(day_of_week)
            # print("---: current data")
            # for item in data_all:
            #     print(item.id_tele)
            #     print(item.type_chat)
            #     print(item.name)
            #     print("- - - -")
            worksheet = get_worksheet()
            if len(values_list_mon) == 0:
                get_all_data_from_gg(worksheet)
            if len(data_all) == 0:
                get_all_data_from_database()
            if day_of_week >= 1 and day_of_week <= 5:
                # if day_of_week == 1 and current_time == "11:14":
                    # db.recyle_all_shipper_list()
                # send data order at 11:15
                if current_time == "11:14":
                    already_sent_money = False
                if current_time == "11:15":
                    sent_money = False
                    for item in data_all:
                        if item.type_chat == "group" or item.type_chat == "supergroup":
                            if already_sent_money == False:
                                display_all_today_order(item.id_tele, worksheet, 2)
                                sent_money = True
                                
                    if sent_money == True:
                        already_sent_money = True

                # send data order at 12:00
                if current_time == "11:58":
                    already_sent = False
                if current_time == "11:59":
                    sent = False
                    for item in data_all:
                        if item.type_chat == "group" or item.type_chat == "supergroup":
                            if already_sent == False:
                                display_all_today_order(item.id_tele, worksheet, 1)
                                sent = True
                                
                    if sent == True:
                        already_sent = True

            updates = get_updates(last_update_id)
            if updates != None:
                if len(updates["result"]) > 0:
                    last_update_id = get_last_update_id(updates) + 1
                    api_handler(updates, worksheet)
            time.sleep(0.5)
        except Exception as e:
            print(e)

if __name__ == '__main__':
    main()
